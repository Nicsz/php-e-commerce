<?php
class Config {
	public $secret = "secret";
	public $sitename = "Films.ua";
	public $address = "http://php-internet-shop.loc/";
	public $address_admin = "http://php-internet-shop.loc/admin/";
	public $db_host = "localhost";
	public $db_user = "root";
	public $db_password = "root";
	public $db_name = "php-internet-shop";
	public $db_prefix = "sdvd_";
	public $sym_query = "{?}";
	
	public $admname = "Vlad";
	public $admemail = "admin@gmail.com";
	public $adm_login = "Admin";
	public $adm_password = "password";//
	
	public $count_on_page = 8;
	public $count_others = 6;
	
	public $pagination_count = 10;
	
	public $dir_text = "lib/text/";
	public $dir_tmpl = "views/";
	public $dir_tmpl_admin = "admin/views/";
	public $dir_img_products = "assets/images/products/";
	
	public $max_name = 255;
	public $max_title = 255;
	public $max_text = 65535;
	
	public $max_size_img = 102400;
	
}
?>