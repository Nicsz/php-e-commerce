# PHP E-commerce

A PHP E-commerce web application.

## System Requirements

The following are required to function properly.

- PHP 5.3

##Admin/Dashboard Area

####Category Management:

<pre>
-- Add Category;
-- Edit Category;
-- Delete Category;
-- Manage Category.
</pre>

####Product Management:

<pre>
-- Product Image;
-- Set Feature's Product;
-- Product Description;
-- Product Stock Management;
-- Discount Set;
-- Add Product;
-- Edit Product;
-- Delete Product;
-- Manage Product Category. 
</pre>

####Orders Management:

<pre>
-- View All Order;
-- Awaiting Delivery.
</pre>

##Frontend

<pre>
-- Fully Responsive Design;
-- Easy to Use Menu;
-- Delighted Product View Section;
-- Order & payment System;
-- SEO Friendly URL;
-- Easy to order system.
</pre>